import { useEffect } from 'react'
import Head from "next/head";
import ScrollAnimation from 'react-animate-on-scroll';
// import { DefaultSeo } from 'next-seo';
// import DEFAULT_SEO from '../src/seo/default.config';
import Header from '../src/components/header';
import { useRouter } from 'next/router';
import Footer from '../src/components/footer';
import * as gtag from '../lib/gtag';
import '../src/styles/globals.scss';


const App = ({ Component, pageProps }) => {
  const router = useRouter();
  const Layout = Component.layout || (({ children }) => <>{children}</>);

  // useEffect(() => {
  //   document.body.classList?.remove('loading')
  // }, []);
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return (
    <>
      {/* <Head /> */}
      {/* <DefaultSeo {...DEFAULT_SEO} /> */}
      <Head />
      <Header />
      <Layout pageProps={pageProps}>
        <main className="relative">
          <Component {...pageProps} />
        </main>
      </Layout>
      <Footer />
    </>
  );
};

export default App;
