const blogData = require("./blog.json");
export default function handler(req, res) {
    res.status(200).json(blogData);
};