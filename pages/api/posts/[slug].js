const blogData = require("./blog.json");
export default function handler(req, res) {
    const { slug } = req.query;
    const post = blogData.filter(el=>el.slug===slug)[0];
    if(post) res.status(200).json(post);
    else res.status(405).end();
};