// import { NextSeo } from 'next-seo';
// import Image from 'next/image';
import fetch from "node-fetch";
import POSTS from  '../api/posts/blog.json';


const isProd = true;
const siteUrl = isProd ? "http://xcapade.in" : "http://localhost:3000";

const style = {
    legendPost: `xm:hidden xp-container xp-content xp-legend lg:grid lg:grid-cols-2 lg:gap-12`,
    allPosts: `xp-container`,
    listHeader: `xm:hidden lg:block xp-border border-b`,
    postList: `mt-8 grid gap-y-16 gap-x-20 xm:grid-cols-1 md:grid-cols-2 xl:grid-cols-3 xl:gap-x-32`,
};


export default ({ allPosts }) => {
    const heroPost = allPosts ? allPosts.filter(el=>el.id==="3")[0] : {};
    // const heroPost = allPosts ? allPosts[0] : {};
    // const morePosts = allPosts ? allPosts.slice(1) : [];
    // console.log(heroPost);
    return (
        <>

            <div className={style.legendPost}>
                <a href={`/blog/${heroPost.slug}`}>
                    <div className="xp-legend-image">
                        <img src={heroPost.image.url} alt="" />
                    </div>
                </a>
                <div>
                    <div>
                        {
                            heroPost.tags.map(el => (<div className="xp-tag mr-2">
                                {el}
                            </div>))
                        }
                    </div>
                    <h1 className="py-4">
                        {heroPost.title}
                    </h1>
                    <p className="py-4 m-0">
                        {heroPost.note}
                    </p>
                    <a href={`/blog/${heroPost.slug}`}>Read article</a>
                </div>
            </div>


            {allPosts.length > 0 && (<div className={style.allPosts}>
                <div className={style.listHeader}>
                    <h2>All articles</h2>
                </div>
                <ul className={style.postList}>
                    {allPosts.map(el => (
                        <li>
                            <div className="post-item">
                                {
                                    el.tags.map(el => (<div className="xp-tag mb-4 mr-2">
                                        {el}
                                    </div>))
                                }
                                <div className="mb-8">
                                    <a href={`/blog/${el.slug}`}>
                                        <img src={el.image.url} alt="" className="xp-list-image" />
                                    </a>
                                </div>
                                <h3 className="mb-4">
                                {el.title}
                                </h3>
                                <p className="m-0 pb-4">
                                {el.note}
                                </p>
                                <a href={`/blog/${el.slug}`} className="block">Read article</a>
                            </div>
                        </li>)
                    )}
                </ul>
            </div>)}

        </>
    );
};

export async function getStaticProps() {
    // try {
    //     const res = (await fetch(`/api/posts`)) || []; //${siteUrl}
    //     const allPosts = await res.json();
    //     console.log(allPosts);
    //     return {
    //         props: { allPosts },
    //     }
    // } catch {
    //     //res.statusCode = 404;
    //     return {
    //         props: {}
    //     };
    // }
    // console.log(POSTS);
    return {
        props : {
            allPosts : POSTS,
        },
    }
}