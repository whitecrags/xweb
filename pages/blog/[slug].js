// import { NextSeo } from 'next-seo';
// import { NextPage, GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import ErrorPage from 'next/error';
import fetch from "node-fetch";
// import fs from "fs";
// import remark from 'remark'
// import html from 'remark-html'
// const fs = require("fs");
// const htm = require('../../blogHTML/3').default.html;
import C4 from '../../src/blogPosts/c4';
import C3 from '../../src/blogPosts/c3';
import C1 from '../../src/blogPosts/c1';
import C2 from '../../src/blogPosts/c2';
import POSTS from '../api/posts/blog.json';

const displayPost = (id) => {
    switch (id) {
        case "1": return <C1 />;
        case "2": return <C2 />;
        case "3": return <C3 />;
        case "4": return <C4 />;
    }
}

const isProd = true;
const siteUrl = isProd ? "http://xcapade.in" : "http://localhost:3000";

const style = {
    mainBlogPost: `xp-container xp-blog-content`,
};


export default ({ post }) => {
    const router = useRouter();
    // if (!router.isFallback && !post?.slug) {
    //     return <ErrorPage statusCode={404} />
    // }
    // console.log(post);
    return (
        <>
            <div className={style.mainBlogPost}>
                {displayPost(post.id)}
            </div>
        </>
    )
};

export async function getStaticProps({ params }) {
    // try {
    //     const res = (await fetch(`${siteUrl}/api/posts/${params.slug}`)) || {};
    //     const post = await res.json();
    //     console.log(post)
    //     return {
    //         props: {
    //             post,
    //         },
    //     }
    // } catch {
    //     return {
    //         props: {}
    //     };
    // }
    const post = POSTS.filter(el=>el.slug===params.slug)[0];
    return {
        props: {
            post,
        },
    };
};

export async function getStaticPaths() {
    // const res = (await fetch(`${siteUrl}/api/posts`)) || [];
    // const allPosts = await res.json();
    const allPosts = POSTS;
    return {
        paths: allPosts.map((el) => `/blog/${el.slug}`),
        fallback: false,
    }
};
