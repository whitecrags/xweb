module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [
    './src/components/**/*.js',
    './pages/**/*.js',
  ],
  theme: {
    screens: {
      xm: '320px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
    },
    height: {
      '6vh': '60vh',
      '3vh': '30vh',
    },
    colors: {
      black: '#000000',
      white: '#ffffff',
      blackDark: '#1e272e',
      blackLight: '#485460',
      greyDark: '#808e9b',
      greyLight: '#d2dae2',
      yellow: '#FFC048',
      green: '#05C46B',
    },
    extend: {
      transitionDuration: {
        '4': '400ms',
      },
      fontFamily: {
        'heading': ['"Poppins"', 'Helvetica', 'Arial', 'sans-serif'],
      }
    }
  },
  variants: {},
  plugins: [
    require('tailwindcss'),
    require('precss'),
    require('autoprefixer')
  ],
}
