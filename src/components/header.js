import cx from 'classnames';

const style = {
    header: `bg-transparent h-20`,
    nav: `xp-container flex h-full items-center`,
    logo: `flex-1`,
    img: `object-cover`,
    sidenav: {
        open: `w-7/12 md:w-60 text-white overflow-x-hidden`,
        close: `w-0 bg-gray-800 text-white overflow-x-hidden`,
        default: `right-0 h-screen fixed z-20 top-0 transition-all ease duration-300 bg-blackDark`,
    },
    item: `flex justify-start cursor-pointer font-medium hover:text-gray-400 ml-8 mb-6`,
    closeIcon: `absolute top-3 focus:outline-none right-3 text-white cursor-pointer`,
};

export default () => {
    return (
        <header className={style.header}>
            <nav className={style.nav}>
                <div className={style.logo}>
                    <a className="flex-shrink-0" href="/blog">
                        <img className={cx(style.img, `xp-logo-icon`)} src="/i/logo/logo.svg" alt="Xcapade" />
                    </a>
                </div>
                <div className="flex items-center">
                    <div className="xp-page-title">
                        blog.
                    </div>
                </div>
            </nav>
        </header>
    );
};
