export default () => {
    const isProd = true;
    const siteUrl = isProd ? "http://xcapade.in" : "http://localhost:3000";

    return (
        <>
            <div className="xp-tag-list">
                <div className="xp-tag"> Snowboarding</div>
            </div>
            <h1 className="py-4"> Snowboarding at a Glance</h1>
            <div className="pt-4">
                <p className="xp-blog-intro"> From “punks'' to athletes to Olympians...Snowboarding has come a long way from whence it began. This is a brief history and present day overview of what's happening in the world of Snowboarding.</p>
                <div className="xp-blog-image"> <img src={`${siteUrl}/i/blog/1/1.jpg`} alt /></div>
                <p> The idea of Snowboarding originally came from a Surfer who simply wanted to glide on snow as he did on water. As if guided by the spirit of <i>Frankenstien</i> himself, <a href="https://www.snowboarder.com/news/sherman-poppen-snurfer-inventor-and-forefather-of-snowboarding-passes-away-at-89/" target="_blank">Sherman Poppen</a> on Christmas day in 1965, during a snowstorm nonetheless, took his daughter’s skis and bolted them together and <i>voila</i> we had the world’s first snowboard - the Snurfer!</p>
                <div className="xp-blog-half-image"> <img src={`${siteUrl}/i/blog/4/2.jpg`} alt /></div>
                <p> And thus started the genesis of a sport that would one day be globally recognised premiering in the Olympics. Was it smooth sailing getting there...maybe not.</p>
                <p> In the formative years, when Snowboarding was kind of getting figured out, a strife emerged between the Snowboarding progenitors and the long existing skiing community. Basically what happened is, up till now (we’re talking about the United States in the 1970’s) for as long as people pictured any activity in snow - it was ONLY Skiing.
                    <br />Now, all of a sudden there was this new thing which entered this snow laden space, a space which had been so far traditionally and unequivocally secured by Skiing and Skiing alone. The environment was such that Snowboarding was being perceived as <i>the new kid on the block.</i>
                    <br />And boy was it pressured! Snowboarding was pressured to an extent that Snowboarders were not permitted in Ski resorts and were labelled as “punks” - <i>Quite literally a shoo(t) at sight order!
                        Uncle Sam just didn’t want them thugs around his Country Club scenes and those baggy pants didn’t help.</i></p>
                <div className="xp-blog-half-image"> <img src={`${siteUrl}/i/blog/4/3.png`} alt /></div>
                <p> The very first Snowboarders had to “sneakily come in at night, hike up the trails, and ride down secretly in order to avoid any penalty.”
                    <br /><i>Full on vigilante vibes!</i></p>
                <p> With growing popularity towards Snowboarding attention towards the <i>Snowboards</i> now fast tracked and many Snowboard manufacturing companies started emerging and setting up shop - design, innovation and progression all seeped in and from then on there has been an industry around Snowboarding. Companies such <a href="https://www.burton.com/us/en/home" target="_blank">Burton</a>, <a href="https://www.simscollective.com/" target="_blank">Sims</a>, <a href="https://www.gnu.com/" target="_blank">GNU</a>, <a href="https://www.rei.com/c/snowboards" target="_blank">REI</a>...founders of each, were either into Skateboarding, Surfing, <i>Snurfing</i> or all three! </p>
                <div className="xp-blog-half-image"> <img src={`${siteUrl}/i/blog/4/4.jpg`} alt /></div>
                <p> Since then, Snowboarding has come a long way from being a Surfer’s experimental venture to establishing its own independent identity, to now, where it is partaken around the world and is an integral part of various premiere competitions, including the Olympics.</p>
                <p> There are multiple disciplines in the field of Snowboarding but primarily there are 2 diverging contingents - recreational (freeriding) and competitive.</p>

                <p> <b>Freeriding</b>
                    <br />Freeriding is all about making the ‘world your oyster’ kind of a setup. The philosophy behind Freeriding Snowboarding is that you can go wherever you want and ride down whichever slope you wish. It is all about going <i>off track</i> into the wilderness or ‘off-piste’ and even exploring the unexplored. Many professional Snowboarders dedicate bulk of their time towards such endeavours and produce content wherein such exploits are showcased both for visibility and credibility.
                    <br /><i>And of course for their sponsors who pay them!</i></p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/NKHYEOAbFyM?start=23" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> Freeriding as expressed by many off-piste Snowboarders is that it symbolises freedom and togetherness in the pursuit of exploration and just having plain, simple, old fashioned FUN!
                    <br /><i>And these, ladies and gentlemen are the hippies of Snowboarding.</i></p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/n0F6hSpxaFc?start=40" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> From a competitive standpoint Snowboarding has numerous events. In Snowboarding competitions there are multiple events of various Snowboarding styles that comprise the entire competition as a whole.</p>

                <p> <b>Halfpipe</b>
                    <br />It is the mother load of all Snowboarding events in terms of popularity - basically the first thought when someone thinks of competitive Snowboarding it is the Halfpipe.
                    <br /> It is this slanting semi cylindrical tube of snow and ice where Snowboarders do tricks shooting from one side to the other. Ranking is given by a panel of judges who grade the athletes based  on the technical difficulty of the performed tricks, their execution, and the height and style exhibited while performing them. Tricks are performed based on the preference of the Snowboarder, in a freestyle manner.
                    <br /> One of the most decorated athletes in Snowboarding - <a href="https://en.wikipedia.org/wiki/Shaun_White" target="_blank">Shaun White</a>, performs in this category. </p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/yKovI9hMjBs?start=11" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>

                <p> <b>Cross</b>
                    <br />The Cross event is an all out race pretty much like the <a href="https://en.wikipedia.org/wiki/Motocross" target="_blank">Motocross event</a>. It is basically 4-6 Snowboarders riding down the mountain through sharp turns, steep and flat sections, various types of jumps, berms, rollers and drops designed specifically to challenge the riders' ability to stay in control while maintaining maximum velocity.</p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/oWcmTQ1tWbI?controls=0&amp;start=68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>

                <p> <b>Slopestyle</b>
                    <br />This is a freestyle form of Snowboarding. In Slopestyle athletes enter an area where they perform various kinds of tricks in a single downhill run - spins, flips, grinds, grabs, you name it!
                    <br />The Slopestyle course consists of three to four large jumps and just as many ramp-like structures. The course is designed keeping in mind to test the riders creativity and consistency and a panel of judges grade the athletes’ skill execution.
                    <br /><i>Think park but instead of swings and stuff there are these big ramps made of snow and the occasional railing!</i></p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/MWte7YpCVUo?controls=0&amp;start=716" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>

                <p> <b>Slalom</b>
                    <br />It is the racing format adopted from Skiing. In Slalom two athletes ‘drop-in’ parallel to each other and have to Snowboard between sets of poles known as ‘gates’. There are 2 separate routes with different Gates accommodating each rider. The athletes completing the circuit (without violation) in the shortest span of time wins or progresses onwards. There are 2 sub categories in Slalom - parallel and grand. The difference between is that the latter has Gates placed more distantly.</p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/bI-FhmBqy9c?start=14" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> Well that's about the gist of it.
                    <br />There are many Snowboarding tournaments and documentaries where professional Snowboarders are constantly pushing the limits within the realm of what is possible and what is eventual. There are many premiere championships and documentaries that can be checked out, some tournaments such as the <a href="https://olympics.com/" target="_blank">Olympics</a>, <a href="http://www.xgames.com/" target="_blank">X-Games</a>, <a href="https://events.burton.com/burton-us-open/" target="_blank">Burton US Open</a> amongst others and documentaries like <a href="https://www.youtube.com/watch?v=kh29_SERH0Y" target="_blank">The Art of Flight</a>, <a href="https://www.youtube.com/watch?v=qXhUSOEykfI" target="_blank">Full Moon</a>, <a href="https://www.youtube.com/watch?v=cS2se-tDLWU" target="_blank">BOOM</a>, amongst others.</p>
            </div>
        </>
    )
};