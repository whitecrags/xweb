export default () => {
    const isProd = true;
    const siteUrl = isProd ? "http://xcapade.in" : "http://localhost:3000";

    return (
        <>
            <div  className="xp-tag-list">
                <div className="xp-tag"> Acro Games</div>
            </div>
            <h1 className="py-4"> Acro Games</h1>
            <div className="pt-4">
                <p className="xp-blog-intro"> A 2 day long acrobatic Paragliding competition between some of the best pilots in the world. <i>Acrobatic whaaaa?!</i> Acrobatic Paragliding is a form of freestyle flying with a glider basically it is the one where you see Paragliders performing all kinds of crazy tricks - and this is where they compete with each other! </p>
                <div className="xp-blog-image"> <img src={`${siteUrl}/i/blog/2/1.jpg`} alt /></div>
                <p> <b>The Acro Games</b> - it is the ultimate showdown of acrobatic skills in Paragliding. An invitational competition held between the best acrobatic pilots in the world showcasing their skills in a beatdown format. </p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/2vHTwpGmn0c?start=56" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> The competition is held over the course of 2 days where pilots face off one another, attempting maneuvers and outperforming their competitors. This invitational competition is an yearly marvel held in Organya (Catalonia, Spain). The location is as wondrous as the sport itself, the valley surrounding Organya provides ideal climatic conditions for paragliding and is fondly revered as the “Magic Mountain’.</p>
                <p> The first day is the qualifying round where the pilots come in pairs and perform a maneuver given to them by the judges. The pilot who fails or is less successful than their counterpart receives a letter from the word ACRO, depending on which letter they are at decides whether they are eliminated or not. The pilots that gather all the 4 letters first (A-C-R-O) are ELIMINATED. 
                    <br/><i>Anyone else remember a game we all played with a similar elimination process with a much less subtle end word...why was it always DONKEY! </i>
                </p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/twIDSZ0d7N4?start=84" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> In the end they’re are only 8 pilots left who qualify for the final round. </p>
                <p> The format for the finals is different - the pilots are still paired up but rather than the judges giving the pilots specific maneuvers, the pilots themselves pull off tricks independently and challenge their counterparts. The judges then decide who performed the acrobats more efficiently. Pretty much like a 1 Vs 1 where whoever wins 4 rounds or gets their opponent ACRO’ed first wins and moves onwards.
                    <br/><i>So basically it is a dance-off but with a giant floating wing thousands of feet above the ground!    </i>
                </p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/zb6UXF6dR3g?start=662" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> The competition brings in the likes of some of the best paragliding pilots in the world such as <a href="https://www.horaciollorens.com/en/horacio-llorens-en/" target="_blank">Horacio Llorens</a>, <a href="https://www.redbull.com/in-en/athlete/victor-carrera" target="_blank">Victor a.k.a Bicho Carrera</a>, <a href="https://www.francoisragolski.com/who-i-am" target="_blank">François Ragolski</a>, <a href="https://www.redbull.com/in-en/athlete/rafael-goberna#:~:text=Biography,support%20of%20his%20mother%2C%20Fabiana.&text=2018%20saw%20first%20and%20second,the%20Aerobatic%20Paragliding%20World%20Tour." target="_blank">Rafael Goberna</a>, Cesar Arevalo and <a href="https://fr.wikipedia.org/wiki/Th%C3%A9o_de_Blic" target="_blank">Theo De Blic</a> amongst others. The competition witnesses the likes of some of the most awe inspiring maneuvers known to the paragliding world.</p>
                <p> Following are some basic ACRO maneuvers with names:</p>
                <div className="xp-blog-media">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/ECggWZyPUhk?start=48" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> During the games many combinations to these maneuvers were showcased and many even displayed unique movements and playstyles highlighting individual competencies and sport expertise. 
                    <br/>Combos such as the Twisted Unnatural Looping to Twisted Opposite MacTwist to Helico or the Twisted SAT to Twisted Joker and the Opposite Twisted MacTwist to Helico… 
                </p>
                <p> Check out the latest Acro Games in the link below
                    <br/><a href="https://www.youtube.com/watch?v=zb6UXF6dR3g&t=333s" target="_blank">ACRO Games 2019 </a>
                </p>
            </div>
        </>
            )
};