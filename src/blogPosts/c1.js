export default () => {
    const isProd = true;
    const siteUrl = isProd ? "http://xcapade.in" : "http://localhost:3000";
    return (
        <>
            <div className="xp-tag-list">
                <div className="xp-tag"> Surfing</div>
            </div>
            <h1 className="py-4"> Surfing on Giants</h1>
            <div className="pt-4">
                <p className="xp-blog-intro"> These are the biggest surfable waves on the planet! An insight into the world of Big Wave Surfing and what it takes to ride these <i>watery</i> beasts. </p>
                <div className="xp-blog-image"> <img src={`${siteUrl}/i/blog/4/1.jpg`} alt /></div>
                <p> If the expression, <i> “it doesn’t get any bigger than this”</i> was applied to Surfing, it’d most definitely be Big Wave Surfing! I mean we’re talking about waves going well over 20 ft. in the air, surfers  riding at speeds of 80-90 kms/hr all the while being on the verge of a colossal water thrashing …!</p>
                <p> These are the mavericks within this community of beach bums, the ones who get their kicks by pushing the extremes and it doesn’t get any more extreme than riding big waves in the world of Surfing.</p>
                <div className="xp-blog-media">
                    <iframe src="https://www.youtube.com/embed/GJc4Ir78KdE?start=268" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> The world record for the largest wave ever surfed, till date (2020) is held by <a href="https://en.wikipedia.org/wiki/Rodrigo_Koxa" target="_blank">Rodrigo Koxa</a> who rode an 80 foot giant beating the previously held record by <a href="https://en.wikipedia.org/wiki/Garrett_McNamara" target="_blank">Garrett McNamara</a> which was a 78 foot beast!</p>
                <div className="xp-blog-media">
                    <iframe src="https://player.vimeo.com/video/247505373" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> Meanwhile the biggest wave surfed by a female athlete stands tall at 73.5 ft., a record held by <a href="https://en.wikipedia.org/wiki/Maya_Gabeira" target="_blank">Maya Gabeira</a>.</p>
                <div className="xp-blog-media">
                    <iframe src="https://www.youtube.com/embed/fTuqJE03aH4?start=7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> This pursuit to ride these tidal jugganauts began when a bunch of <a href="https://en.wikipedia.org/wiki/Soul_surfer" target="_blank">soul surfers</a> who wanted to explore a different line of approach towards Surfing (as opposed to competitive setups wherein surfers competed with each other performing maneuvers), these guys wanted to ride BIG waves.</p>
                <p> Catching a big wave is not that simple, they usually occur offshore which means that you need to paddle some distance to get to them, now once you’re at the spot, you need to have enough speed to cross the length of the wave before it collapses and takes you down with it…! And so the concept of <a href="https://en.wikipedia.org/wiki/Tow-in_surfing" target="_blank">Tow-in Surfing</a> was forged (1992).
                    <br /> Here the surfer would be towed from the coast and slinged into the wave, with the help of a <a href="https://en.wikipedia.org/wiki/Personal_watercraft" target="_blank">personal watercraft</a> (a.k.a. jet ski). Basically, a piggyback ride and a booster shot... Also to rescue the surfer if ‘things go south’.
                </p>
                <p> Which brings us to the <i>south</i> - what is the risk involved? Why is it such a big deal to ride a big wave?
                    <br /> And that is a Wipeout. A Wipeout is basically the ‘shit-show’ that follows if you fall off your surfboard in the midst of riding a wave. Now if it was an average water wave (10-15 ft.) the weight and speed, and therefore the force is relatively (<i>much</i>) lesser than a wave that has 5 times that force if it capsizes upon you...some Wipeouts are so severe that they can seriously injure or drown someone.
                </p>
                <div className="xp-blog-media">
                    <iframe src="https://www.youtube.com/embed/TACcusJEeQY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <p> While striving to reach heights never before accomplished and plunging into realms of pristine wilderness, there is always an element of risk but it is this risk that keeps these soul (<i>reaping</i>) surfers ticking. These Big Wave affecianadoes emphasise that beyond the physical rigours of the craft or the efficiency of applied technique, it is the mental challenge of holding onto one’s nerve in moments of life threatening circumstances...<i>simply put these guys have seen way too many surfing movies!</i></p>
            </div>
        </>
    )
};