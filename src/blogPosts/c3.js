export default () => {
    const isProd = true;
    const siteUrl = isProd ? "http://xcapade.in" : "http://localhost:3000";


    return (
        <>
            <div className="xp-tag-list">
                <div className="xp-tag"> Climbing</div>
                <div className="xp-tag"> Mountaineering</div>
            </div>
            <h1 className="py-4"> K2 - The Savage Mountain</h1>
            <div className="pt-4">
                <p className="xp-blog-intro"> K2 is one of the most notorious mountains to have existed on this planet. And, it is not just because it is the 2nd highest peak there is and that climbing it is a daunting enterprise for even the most weathered Mountaineers but rather, that this mountain has more skeletons buried than a graveyard, <i>literally</i>.</p>
                <div className="xp-blog-image"> <img src={`${siteUrl}/i/blog/3/1.jpg`} alt /></div>
                <p> More people have gone into outer space than have stood on top of this mountain...and lived to tell the tale. To this date (<i>2021! Lest we forget</i>), less than 400 climbers have climbed the mountain while in return the mountain has laid claims to lives 4 times that - Thissss... is not a mountain but a tomb!</p>
                <p> <i>Well at least it’s got lofty standards for a crypt!</i></p>
                <p className="xp-blog-quote">
                    “... just the bare bones of a name, all rock and ice and storm and abyss. It makes no attempt to sound human. It is atoms and stars. It has the nakedness of the world before the first man – or of the cindered planet after the last.”
                    <span className="qauthour">- <a href="https://en.wikipedia.org/wiki/Fosco_Maraini" target="_blank">Fosco Maraini</a></span>
                </p>
                <p> Having risen to an altitude of 8,611 meters (28,251 ft.), K2 is regarded as an absolute test for a Mountaineer and it is said with good measure - It is a beast of a mountain.
                    <br /> It is a combination of all the deadly sins associated with mountain climbing;
                    <br />
                    <ul>
                        <li> <b>God awful weather</b> (even from frigid mountain standards)
                            <br /> Gusts of wind going upto 200 km/hr and temperatures going as far under as -60℃</li>
                        <li> <b>Completely isolated</b>
                            <br /> The nearest town (<a href="https://en.wikipedia.org/wiki/Askole" target="_blank">Askole</a>) is 66.6 kms away! <i>Like there weren’t any less eerie signs already!</i></li>
                        <li> <b>Technical command</b>
                            <br /> K2 really puts the ‘T’ in technicality as far as Mountaineering goes. The Mountain is this near vertical pillar of bare rock with streaks of ice and snow demanding every ounce of the climbers’ technical prowess.</li>
                        <li> <b>The <i>Death Zone</i></b>
                            <br /> This is why climbing 8,000+ meter peaks is a thing. The death zone - it is categorically classified as a place incapable of supporting human life. It is an elevation zone where the levels of oxygen in the air get so low (nearly one-third found at sea level) that it chokes up our bodily functions just to keep our oxygen flowing.</li>
                    </ul>
                </p>
                <p><i>So people actually go to these places, like on purpose?!</i></p>
                <div className="xp-blog-media">
                    <iframe src="https://www.youtube.com/embed/zlEDYZI2OGM?start=3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen />
                </div>
                <p> The origin of recreational climbing - <i>and by recreational it means a self-sabotaging pastime that has you constantly battling for survival... Cool, just as long as we’re on the same page</i> - can be traced back as early as the 18th century when someone put a reward out for climbing <a href="https://en.wikipedia.org/wiki/Mont_Blanc" target="_blank">Mont Blanc</a>, and from then on mankind was hooked.</p>
                <p> More than 250 years of mountain climbing and still the reason for doing it is as perplexing as <i>John Cena saying “you can’t see me”</i>, but what has undisputedly emerged and generally agreed upon (through scores of documented interviews) amongst Mountaineers is that beyond the evident physical rigours there lies a far more fierce reckoning - the mental fortitude and endurance to just keep going on! The chance to be put to the test and showcase this indomitable spirit is what keeps these guys going.
                    <br /> <i>So it’s like this weirdddddd fetish...which makes it niche! Well played.</i></p>
                <p className="xp-blog-quote">
                    “We force ourselves against our instinct of survival, our strongest instinct. An egotistical instinct (instead) is our instinct of survival. We have it.”
                    <span className="qauthour"> -<a href="https://en.wikipedia.org/wiki/Reinhold_Messner" target="_blank">Reinhold Messner</a></span>
                </p>
                <p> There are in fact multiple ways to climb a mountain, any mountain, you just pick which side you wanna climb from and just go with it but this is where things get interesting - climbing from one side and summiting is not the same as to have climbed all the sides of a mountain, so it is a geological probability that the terrain on one side can be much more technically challenging than the one/s that have been climbed already and THAT is what modern day Mountaineering is all about - making the <i>perceived</i> impossible, possible.</p>
                <div className="xp-blog-half-image"> <img src={`${siteUrl}/i/blog/3/2.jpg`} alt /></div>
                <p> Now the East and the North facing sides of K2 remain unclimbed, to this date. Of the routes upon which ascents have been made though, there are these two in particular - the “Polish Line” or “Central Rib” and the “Magic Line” - they both neighbour each other. Till date the Polish Line has never been repeated and the duo who did it (<i>look it up!</i>), one of them sadly could not make it back alive, while the Magic Line has a devastating fatality rate and has only been repeated twice but with mass casualties.
                    <br />That’s the thing about K2 and why it is notoriously known to be the <i><a href="https://www.nationalgeographic.com/adventure/article/151213-k2-adventure-mountain-climb-china-pakistan-disaster-ngbooktalk" target="_blank">Savage Mountain</a></i>, climbing it is like getting the <i>Soul Stone - a soul for a climb, apparently!</i></p>
                <div className="xp-blog-half-image"> <img src={`${siteUrl}/i/blog/3/3.jpg`} alt /></div>
                <p> Also, neither one is reckoned to be the hardest climbing route on K2, it is in fact - the North Ridge that takes that claim, <i>just so you all know.</i><br />
                    While the most commonly used route, is the Abruzzi Spur route. It is named after <a href="https://en.wikipedia.org/wiki/Prince_Luigi_Amedeo,_Duke_of_the_Abruzzi" target="_blank">Prince Luigi Amedeo, Duke of the Abruzzi</a> who was the first to try and climb the mountain using this route way back in year 1909. Though the route was discovered the mountain did not yield that day and it was all the way in 1954 that the first assent on K2 was made by two Italian climbers  <a href="https://en.wikipedia.org/wiki/Lino_Lacedelli" target="_blank">Lino Lacedelli</a>  and  <a href="https://en.wikipedia.org/wiki/Achille_Compagnoni" target="_blank">Achille Compagnoni</a>.<br /><i>Viva L'Italia</i></p>
                {/* <div className="xp-blog-media">
                    <iframe src="https://www.youtube.com/embed/hC-4ZGpk3J8?start=157" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen />
                </div> */}
                <p> The most recent notable feat on K2 was, when a team of 10 Napalese climbers made the <a href="https://www.msn.com/en-us/news/world/nepali-mountaineers-achieve-historic-winter-first-on-k2/ar-BB1cOfjx" target="_blank">first ever <i>winter</i> summit</a> on January 16, 2021.</p>
                {/* <div className="xp-blog-media">
                    <iframe src="https://www.youtube.com/embed/TDNnKLY0h8Q?start=12" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen />
                </div> */}
                <p> Despite the dangers lurking within these endeavours it is the strength of the human spirit that we celebrate and so we celebrate these weirdos and their unyielding egos that defy all logical reasoning… So here's to you all, just “<i><a href="https://www.goodreads.com/author/quotes/3348511.George_Mallory" target="_blank">because it’s there</a></i>”.</p>
            </div>
        </>
    )
};